fun main(){
    val number: Byte = 8
    println(number::class.simpleName)

    val numberTwo = 800000000000
    println(numberTwo.toByte()::class.simpleName)

    val numberThree = 8.12
    println(numberThree::class.simpleName)

    val numberFour = 8.12F
    println(numberFour::class.simpleName)

    val numberFive: Int = numberFour as Int
    println(numberFive::class.simpleName)
    //byte < short < int < long < float < double
}