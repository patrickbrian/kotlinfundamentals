//Deposit
//1. You cannot deposit more than 5000
//2. You cannot deposit an amount not divisible by 100.
//3. You cannot deposit negative amount.

//Withdraw
//1. You cannot withdraw more than half of the current amount
//2. you cannot withdraw negative amount

fun main(args: Array<String>){
    val account: MutableMap<String, Any> = mutableMapOf(
        "username" to "Brandon",
        "balance" to 0
    )
    println(checkBalance(account))
    println(deposit(account, 5001))
    println(deposit(account, 5000))
    println(deposit(account, 4001))
    println(deposit(account, 500))
    println(deposit(account, -100))
    withdraw(account, 10000)
    withdraw(account, 200)
    withdraw(account, -100)
}

//function to check the balance
fun checkBalance(account: Map<String, Any>): String {
    return "Your balance is: ${account.getValue("balance")}"
}

//function to deposit
fun deposit(account: MutableMap<String, Any>, amount: Int): String{
    if(amount > 5000) return "You cannot deposit more than 5000."
    else if (amount % 100 != 0) return "You cannot deposit an amount not divisible by 100."
    else if(amount < 0) return "You cannot deposit negative amount."
    else {
        var newBalance = account.getValue("balance") as Int + amount
        account["balance"] = newBalance
        return "You just deposited $amount. Your new balance is: $newBalance."
    }
}

//function to withdraw
fun withdraw(account: MutableMap<String, Any>, amount: Int): Unit {
    if(amount > (account.getValue("balance") as Int/2))
        println("You cannot withdraw more than half of the current amount.")
    else if(amount < 0) println("You cannot withdraw negative amount.")
    else {
        var newBalance = account.getValue("balance") as Int - amount
        account["balance"] = newBalance
        println("Your new balance is $newBalance.")
    }
}

