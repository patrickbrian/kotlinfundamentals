fun main(args: Array<String>){
//    val numbers = listOf(1,2,3,4,5)
//
//    //foreach ->
//    //(variableName ->) to change it variable name
//    numbers.forEach{ number ->
//        val newNumber = number*15
//        println(newNumber)
//    }
//
//    //map-> will return a new collection, performing transformation
//    val newNumbers = numbers.map{
//        it*4
//        it*3
//        it*2
//    }
//    println(newNumbers)
//
//
//    // filter -> will return a new collection, filtering the values based on our condition
//    val evenNumbers = numbers.filter{
//        it % 2 == 0
//    }
//    println(evenNumbers)


    //functions
    val number = 4
//    if(number % 2 == 4) println("Number is even")
//    else println("Number is odd")
//    println(isEven(number))

    val number2 = 3
//    if(number2 % 2 == 4) println("Number is even")
//    else println("Number is odd")
//    println(isEven(number2))

    //functions for ATMs
    //Check balance -> "Your balance is: ____"
    //Deposit -> "You just deposited ____. Your new balance is: ____"
    //Withdrawal -> "Your new balance is: ____"
//
//    val account: MutableMap<String, Any> = mutableMapOf(
//        "username" to "Brandon",
//        "balance" to 0
//    )
//
//    println(checkBalance(account))
//    println(deposit(account, 500))
//    withdraw(account, 200)
//    println(deposit(account, 100))
//    withdraw(account, 50)
}

//function to check whether a number is odd or even
//fun isEven(num: Int): Boolean {
//    return num % 2 == 0
//}
//
////function to check the balance
//fun checkBalance(account: Map<String, Any>): String {
//    return "Your balance is: ${account.getValue("balance")}"
//}
//
////function to deposit
//fun deposit(account: MutableMap<String, Any>, amount: Int): String{
//
//    var newBalance = account.getValue("balance") as Int + amount
//    account["balance"] = newBalance
//
//    return "You just deposited $amount. Your new balance is: $newBalance"
//}
//
////function to withdraw
//fun withdraw(account: MutableMap<String, Any>, amount: Int): Unit {
//    var newBalance = account.getValue("balance") as Int - amount
//    account["balance"] = newBalance
//
//    println("Your new balance is $newBalance")
//}
//
