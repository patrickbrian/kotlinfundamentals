fun main(){
    // > < >= <= ==

    val numOne = 1
    val numTwo = 2

    println("$numOne > $numTwo is ${numOne>numTwo}")
    println("$numOne < $numTwo is ${numOne<numTwo}")
    println("$numOne >= $numTwo is ${numOne>=numTwo}")
    println("$numOne <= $numTwo is ${numOne<=numTwo}")
    println("$numOne == $numTwo is ${numOne==numTwo}")
}