fun main(){
    // arithmetic
    val numByte = 1
    val numShort = 2
    var numLong = 7
    val numFloat = 1.23F
    val numDouble = 4


    var result = numLong + numShort
    println("$numLong + $numShort = $result")

    result = numLong - numShort
    println("$numLong - $numShort = $result")

    result = numLong * numShort
    println("$numLong * $numShort = $result")

    result = numLong / numShort
    println("$numLong / $numShort = $result")

    result = numLong % numShort
    println("$numLong % $numShort = $result")

    println(numLong++)
    println(numLong--)

    numLong += numLong
    println(numLong)

    numLong -= numLong
    println(numLong)

    numLong *= numLong
    println(numLong)

    numLong /= numLong
    println(numLong)
    // logical
    // comparison
    // assignment
    // unary or augmented
    // boolean
}