fun main(){
    val isNotChristmas = false
    val isChristmas = true

    // t and t = t
    // t and f = f
    // f and t = f
    // f and f = f

    println(isChristmas && isChristmas)
    println(!isChristmas && !isChristmas)
    println(!isChristmas && isChristmas)
    println(isChristmas && !isChristmas)

    // t or t = t
    // t or f = t
    // f or t = t
    // f or f = f

    println(isChristmas || isChristmas)
    println(!isChristmas || !isChristmas)
    println(!isChristmas || isChristmas)
    println(isChristmas || !isChristmas)
}