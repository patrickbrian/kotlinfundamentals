// val grades = listOf(
//      listOf(94,82,85),
//      listOf(83,99,97),
//      listOf(76,89,90)
// )

//1. Create a loop, that will return the highest grade of each subject.


fun main(args: Array<String>){
    val grades = listOf(
        listOf(94,82,85),
        listOf(83,99,97),
        listOf(76,89,90)
    )

    grades.forEach{ grade ->
        println(grade.maxOrNull() ?: 0)
    }
}