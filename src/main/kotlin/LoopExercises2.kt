// Using filter, given a list from 1-1000, display all the perfect numbers.

fun main(args: Array<String>){
    val numbers = (1 .. 1000).toList()

    val perfectNumbers = numbers.filter{ number ->
        var summation = 0
        for(index in 1 until number) {
            if (number % index == 0)
                summation += index
        }

        summation == number
    }
    println(perfectNumbers)

//    val perfectNumbers = numbers.filter{ number ->
//        (1 until number).toList().filter{ index ->
//            number % index == 0
//        }.sum() == number
//    }
//    println(perfectNumbers)
}