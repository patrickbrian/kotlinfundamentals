import java.lang.IllegalArgumentException

fun main(){

    var message = ""
    try{
        message = "Welcome to Kotlin Tutorials"
        message.toInt()
    }catch(e: NumberFormatException){
        e.printStackTrace()
        println(e.message)
    }finally {
        println("with errors")
    }

    if(message.length > 10)
        throw IllegalArgumentException()

}